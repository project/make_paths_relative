INTRODUCTION
------------

This module can make(convert) the paths(URLs) to relative instead of absolute.
It is useful to using the relative URLs. You can find the more details, here:
https://support.google.com/analytics/answer/2664470

For a full description of the module, visit the project page:
  http://drupal.org/project/make_paths_relative

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/make_paths_relative


-- REQUIREMENTS --

None.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.

CONFIGURATION
-------------

Make settings and activate the functionaity from the settings page.
Setting page found under: Administer -> Configuration -> System
-> Make Paths Relative Settings.


-- CONTACT --

Current maintainers:
* Sami Ahmed Siddiqui (sasiddiqui) - http://drupal.org/user/3100375
